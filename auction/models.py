from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


class Item(models.Model):
    """Model to store the details of the items to be auctioned."""

    name = models.CharField(max_length=100)
    description = models.TextField()
    owner = models.ForeignKey(User)

    def __str__(self):
        """Get nice name for the model instance."""
        return self.name


class Auction(models.Model):
    """Model to Store the auction details."""

    published_at = models.DateTimeField(auto_now_add=True)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    starting_amount = models.FloatField(default=100.0)
    item = models.ForeignKey(Item)

    def __str__(self):
        """Get nice name for the model instance."""
        return self.item.name

    def is_active(self):
        """Check if the auction is active or not."""
        return (self.end_time >= timezone.now()) and (
            self.start_time <= timezone.now())

    def is_completed(self):
        """Check if the auction is completed or not."""
        return (self.end_time < timezone.now())

    def get_buyer(self):
        """
        Get the details of the buyer.

        Only works if the bidding is closed.
        """
        winning_bid = self.bid_set.filter(is_winner=True)
        if winning_bid:
            return winning_bid.first().bid_by
        else:
            return None

    def get_highest_bid(self):
        """
        Get the highest bid made in the auction.

        Only works if the bidding is closed.
        """
        winning_bid = self.bid_set.filter(is_winner=True)
        if winning_bid:
            return winning_bid.first().bid_amount
        else:
            return None


class Bid(models.Model):
    """Model to store the details of the bid made by users."""

    auction = models.ForeignKey(Auction)
    bid_by = models.ForeignKey(User)
    bid_time = models.DateTimeField(auto_now_add=True)
    bid_amount = models.FloatField(default=100.0)
    is_winner = models.BooleanField(default=False)

    def __str__(self):
        """Get nice name for the model instance."""
        return "%f bid for %s by %s" % (
            self.bid_amount, self.auction, self.bid_by.username)
