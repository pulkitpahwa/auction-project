from __future__ import absolute_import, unicode_literals
from celery import task
from django.utils import timezone
from .models import Auction, Item
from django.contrib.auth.models import User

import datetime


@task()
def task_find_auction_winner():
    """
    Find the winner for the auctions that finished in last 1 hour.

    Find all the auctions that completed in last 1 hour
    and finds the largest bidder for each of the auction.
    """
    now = timezone.now()
    hour_before = now - datetime.timedelta(hours=1)
    recently_closed_auctions = Auction.objects.filter(
        end_time__gt=hour_before, end_time__lte=now).prefetch_related(
        "bid_set")
    for auction in recently_closed_auctions:
        all_bids = auction.bid_set.all().order_by("-bid_amount")
        if all_bids:
            highest_bid = all_bids.first()
            highest_bid.is_winner = True
            highest_bid.save()
