from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from rest_framework import viewsets, status
from rest_framework.response import Response
from .models import Auction, Bid
from .serializers import (
    AuctionSerializer,
    AuctionSerializerWithBuyerDetails,
    BidSerializer,
    MyBidsSerializer,
    UserSerializer)
import datetime


class AuctionViewSet(viewsets.ModelViewSet):
    queryset = Auction.objects.all().prefetch_related("item")
    serializer_class = AuctionSerializer

    def list(self, request):
        search_filter = request.GET.get('filter')
        now = datetime.datetime.now()
        if search_filter == "upcoming":
            self.queryset = self.queryset.filter(start_time__gt=now)
        if search_filter == "previous":
            self.queryset = self.queryset.filter(start_time__lt=now)
        serializer = AuctionSerializer(self.queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = self.queryset
        auction = get_object_or_404(queryset, pk=pk)
        if auction.is_completed():
            serializer = AuctionSerializerWithBuyerDetails(auction)
        else:
            serializer = AuctionSerializer(auction)
        return Response(serializer.data)


class BidsViewSet(viewsets.ModelViewSet):
    queryset = Bid.objects.all().prefetch_related("auction")
    serializer_class = BidSerializer

    def list(self, request):
        if not request.user.is_authenticated():
            return Response(status=status.HTTP_401_UNAUTHORISED)
        user = User.objects.filter(id=request.user.id).prefetch_related(
            "bid_set").first()
        serializer = MyBidsSerializer(
            user.bid_set.all().select_related("auction__item"), many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return Response(status=status.HTTP_401_UNAUTHORISED)
        data = request.data
        try:
            auction = Auction.objects.get(id=data.get('auction'))
        except Auction.DoesNotExist:
            return Response({"auction": "Invalid auction ID"},
                            status=status.HTTP_404_NOT_FOUND)
        amount = data.get("bid_amount")
        if not amount:
            return Response({"bid_amount": "This field is required"},
                            status=status.HTTP_404_NOT_FOUND)
        if amount < auction.starting_amount:
            return Response({"bid_amount": "Bid amount can't be less than min. bid amount"},
                            status=status.HTTP_404_NOT_FOUND)
        Bid.objects.create(bid_by=request.user, auction=auction,
                           bid_amount=amount)
        return Response(status=status.HTTP_201_CREATED)
