from django.contrib.auth.models import User
from rest_framework import serializers

from .models import Auction, Bid, Item


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('get_full_name', 'username', "id")


class ItemSerializer(serializers.ModelSerializer):
    owner = UserSerializer()

    class Meta:
        model = Item
        fields = ('name', 'description', 'owner', "id")


class ItemSummarySerializer(serializers.ModelSerializer):

    class Meta:
        model = Item
        fields = ('name', 'description', "id")


class AuctionSerializer(serializers.ModelSerializer):
    item = ItemSerializer()
    highest_bid = serializers.FloatField(source='get_highest_bid')

    class Meta:
        model = Auction
        fields = ('item', 'published_at', 'start_time', 'highest_bid',
                  'end_time', 'starting_amount', "id", 'is_completed')


class AuctionSummarySerializer(serializers.ModelSerializer):
    item = ItemSummarySerializer()
    highest_bid = serializers.FloatField(source='get_highest_bid')

    class Meta:
        model = Auction
        fields = ('item', 'published_at', 'start_time', 'highest_bid',
                  'end_time', 'starting_amount', "id", 'is_completed')


class AuctionSerializerWithBuyerDetails(serializers.ModelSerializer):
    item = ItemSerializer()
    buyer = UserSerializer(source='get_buyer')
    highest_bid = serializers.FloatField(source='get_highest_bid')

    class Meta:
        model = Auction
        fields = ('item', 'published_at', 'start_time', 'buyer',
                  'end_time', 'starting_amount', "id", 'is_completed',
                  'highest_bid')


class BidSerializer(serializers.ModelSerializer):
    auction = AuctionSerializer()
    bid_by = UserSerializer()

    class Meta:
        model = Bid
        fields = ('auction', 'bid_by', 'bid_time',
                  'bid_amount', 'is_winner', "id")


class MyBidsSerializer(serializers.ModelSerializer):
    item = ItemSummarySerializer(source="auction.item")

    class Meta:
        model = Bid
        fields = ('auction', 'bid_time', "item",
                  'bid_amount', 'is_winner', "id")
