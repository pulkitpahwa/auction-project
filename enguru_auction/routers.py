from rest_framework import routers
from auction.views import BidsViewSet, AuctionViewSet

router = routers.DefaultRouter(trailing_slash=False)

router.register(r'auctions', AuctionViewSet)
router.register(r'bids', BidsViewSet)
