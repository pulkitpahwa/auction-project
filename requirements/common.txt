# Core Stuff
# -------------------------------------
Django==1.10.0
gunicorn==19.6.0

# Configuration
# -------------------------------------
argon2-cffi==16.1.0
django-environ==0.4.0
django-sites==0.9
python-dotenv==0.5.1

# Staticfiles
# -------------------------------------
whitenoise==3.2

# Extensions
# -------------------------------------
pytz==2016.6.1

# Models
# -------------------------------------
psycopg2==2.6.2

Pillow==3.3.0
django-versatileimagefield==1.6
django-uuid-upload-path==1.0.0

# REST APIs
# -------------------------------------
djangorestframework==3.4.3

# LOGGING
# -------------------------------------
django-log-request-id==1.1.0


# scheduling tasks
celery==4.0.2