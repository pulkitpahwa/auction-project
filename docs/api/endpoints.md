For api overview and usages, check out [this page](overview.md).

[TOC]

# Authentication

Authentication for the time being is not based on Token based authentication. The authentication currently uses normal django session based authentication. I have tried to keep things simple in the assignment by just focusing on the functionality of bidding on the existing auctions.

To use django-admins, use django-admin(superuser access is required). If a superuser is not yet available, you can create one by running the command `python manage.py createsuperuser` in the python shell of the root directory of the project. 

You can use django-admin to :
* create/ update/ delete user
* create/ update/ delete item
* create/ update/ delete auction

To create auctions, you can create a superuser and create item followed by creating auction for that particular item. The reason for creating a separate table to store the item details is to allow multiple auctions for the same item(in some cases it might happen that the auction wasn't successful or there were some reasons due to which the auction was supposed to be shut down, so to allow next round of auction)

here are the available endpoints:

1. `/api/auctions`

Get: List all the auctions
you can pass filter as query-parameter to the request to filter *upcoming*, *all*, *previous* auctions

Data:

`/api/auctions`
```
  [
    {
        "item": {
            "name": "Chair",
            "description": "A chair",
            "owner": {
                "get_full_name": "",
                "username": "pulkitpahwa",
                "id": 1
            },
            "id": 1
        },
        "published_at": "2018-05-06T02:46:48.951883Z",
        "start_time": "2018-05-05T08:16:37Z",
        "highest_bid": null,
        "end_time": "2018-05-06T03:16:40Z",
        "starting_amount": 100.0,
        "id": 1,
        "is_completed": true
    },
    {
        "item": {
            "name": "Table",
            "description": "Table",
            "owner": {
                "get_full_name": "",
                "username": "pulkitpahwa",
                "id": 1
            },
            "id": 2
        },
        "published_at": "2018-05-06T02:47:05.139888Z",
        "start_time": "2018-05-06T08:16:51Z",
        "highest_bid": null,
        "end_time": "2018-05-26T08:16:53Z",
        "starting_amount": 1000.0,
        "id": 2,
        "is_completed": false
    }
]
```

`/api/auctions?filter=upcoming`
```
  [
    {
        "item": {
            "name": "Table",
            "description": "Table",
            "owner": {
                "get_full_name": "",
                "username": "pulkitpahwa",
                "id": 1
            },
            "id": 2
        },
        "published_at": "2018-05-06T02:47:05.139888Z",
        "start_time": "2018-05-06T08:16:51Z",
        "highest_bid": null,
        "end_time": "2018-05-26T08:16:53Z",
        "starting_amount": 1000.0,
        "id": 2,
        "is_completed": false
    }
]
```


`/api/auctions/<auction_id>` : get particular auction

```
{
    "item": {
        "name": "Chair",
        "description": "A chair",
        "owner": {
            "get_full_name": "",
            "username": "pulkitpahwa",
            "id": 1
        },
        "id": 1
    },
    "published_at": "2018-05-06T02:46:48.951883Z",
    "start_time": "2018-05-05T08:16:37Z",
    "buyer": null,
    "end_time": "2018-05-06T03:16:40Z",
    "starting_amount": 100.0,
    "id": 1,
    "is_completed": true,
    "highest_bid": null
}```



2. `/api/bids`

Get: List all the bids for the authenticated user

```
[
    {
        "auction": 1,
        "bid_time": "2018-05-06T05:14:54.968281Z",
        "item": {
            "name": "Chair",
            "description": "A chair",
            "id": 1
        },
        "bid_amount": 1000.0,
        "is_winner": false,
        "id": 1
    },
    {
        "auction": 1,
        "bid_time": "2018-05-06T05:50:31.361463Z",
        "item": {
            "name": "Chair",
            "description": "A chair",
            "id": 1
        },
        "bid_amount": 2.0,
        "is_winner": false,
        "id": 2
    }
]
```

`/api/bids/<bid_id>` : get particular bid

```{
    "auction": {
        "item": {
            "name": "Chair",
            "description": "A chair",
            "owner": {
                "get_full_name": "",
                "username": "pulkitpahwa",
                "id": 1
            },
            "id": 1
        },
        "published_at": "2018-05-06T02:46:48.951883Z",
        "start_time": "2018-05-05T08:16:37Z",
        "highest_bid": null,
        "end_time": "2018-05-06T03:16:40Z",
        "starting_amount": 100.0,
        "id": 1,
        "is_completed": true
    },
    "bid_by": {
        "get_full_name": "",
        "username": "pulkitpahwa",
        "id": 1
    },
    "bid_time": "2018-05-06T05:14:54.968281Z",
    "bid_amount": 1000.0,
    "is_winner": false,
    "id": 1
}```

Request MEthod : POST

`/api/bids` : create new bid for the given auction

Data: ```{
    "auction": 1,
    "bid_amount": 1000
}```

Response: 201 (status-code) (created)

